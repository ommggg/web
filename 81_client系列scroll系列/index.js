window.addEventListener('load', function () {
    var box1 = document.querySelector('.box1');
    console.log('offsetHeight:' + box1.offsetHeight);
    console.log('clientHeight:' + box1.clientHeight);
    console.log('scrollHeight:' + box1.scrollHeight);

    box1.addEventListener('scroll', function () {
        console.log('scrollTop:' + box1.scrollTop);
    })

    var sliderbar = document.querySelector('.slider-bar');
    var top = sliderbar.querySelector('div');
    var bannerTop = 250; //滚动节点（滚出长度）
    var sliderbarTop = sliderbar.offsetTop - bannerTop; //记录当时的位置
    document.addEventListener('scroll', function () {
        if (window.pageYOffset > bannerTop) {
            sliderbar.style.position = 'fixed';
            sliderbar.style.top = sliderbarTop + 'px';
        } else {
            sliderbar.style.position = 'absolute';
            sliderbar.style.top = 300 + 'px';
        }
    })

    // 跳转到顶部
    top.addEventListener('click', function () {
        animateY(window, 0);
    })

    var move = document.querySelector('.move');
    var move2 = document.querySelector('.move2');
    var move2btn1 = document.querySelector('.move2btn1');
    var move2btn2 = document.querySelector('.move2btn2');
    animate(move, 300);

    move2btn1.addEventListener('click', function () {
        animate(move2, 500, function () {
            move2.style.backgroundColor = 'pink';
        });
    })
    move2btn2.addEventListener('click', function () {
        animate(move2, 800, function () {
            move2.style.backgroundColor = 'red';
        });
    })

    // 封装一个动画函数 animate(a,b) a:元素 b:移动距离
    // function animate(a, b) {
    //     clearInterval(a.timer);
    //     a.timer = this.setInterval(function () {
    //         if (a.offsetLeft == b) {
    //             clearInterval(a.timer);
    //         }
    //         a.style.left = a.offsetLeft + 1 + 'px';
    //     }, 5);
    // }

    // function animate2(a, b) {
    //     clearInterval(a.timer);
    //     a.timer = this.setInterval(function () {
    //         if (a.offsetLeft == b) {
    //             clearInterval(a.timer);
    //         }
    //         var step = ((b - a.offsetLeft) / 10);
    //         step = step > 0 ? Math.ceil(step) : Math.floor(step);
    //         a.style.left = a.offsetLeft + step + 'px';
    //         console.log(a.offsetLeft);
    //     }, 15);
    // }

    var slideBar = document.querySelector('.sliderbar');
    var con = document.querySelector('.con');
    slideBar.addEventListener('mouseenter', function () {
        animate(con, -100);
        slideBar.children[0].innerHTML = '→'; //立刻执行
    })
    slideBar.addEventListener('mouseleave', function () {
        animate(con, 0, function () {
            slideBar.children[0].innerHTML = '←'; //回调函数
        })
    })
})