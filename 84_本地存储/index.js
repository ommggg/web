window.addEventListener('load', function () {
    // sessionStorage
    var text1 = document.querySelector('#text1');
    var text2 = document.querySelector('#text2');
    var set = document.querySelector('.set');
    var get = document.querySelector('.get');
    var remove = document.querySelector('.remove');
    var clear = document.querySelector('.clear');

    set.addEventListener('click', function () {
        var textvalue1 = text1.value;
        var textvalue2 = text2.value;
        sessionStorage.setItem('' + textvalue1, textvalue2);
    })
    get.addEventListener('click', function () {
        var textvalue1 = text1.value;
        var uname = sessionStorage.getItem('' + textvalue1);
        alert(uname);
    })
    remove.addEventListener('click', function () {
        var textvalue1 = text1.value;
        sessionStorage.removeItem('' + textvalue1);
    })
    clear.addEventListener('click', function () {
        sessionStorage.clear();
    })

    //localStorage
    var text3 = document.querySelector('#text3');
    var text4 = document.querySelector('#text4');
    var set1 = document.querySelector('.set1');
    var get1 = document.querySelector('.get1');
    var remove1 = document.querySelector('.remove1');
    var clear1 = document.querySelector('.clear1');

    set1.addEventListener('click', function () {
        var textvalue3 = text3.value;
        var textvalue4 = text4.value;
        localStorage.setItem('' + textvalue3, textvalue4);
    })
    get1.addEventListener('click', function () {
        var textvalue3 = text3.value;
        var uname1 = localStorage.getItem('' + textvalue3);
        alert(uname1);
    })
    remove1.addEventListener('click', function () {
        var textvalue3 = text3.value;
        localStorage.removeItem('' + textvalue3);
    })
    clear1.addEventListener('click', function () {
        localStorage.clear();
    })

    var ipt = document.querySelector('#ipt');
    var check = document.querySelector('#check');
    if (localStorage.getItem('username')) {
        ipt.value = localStorage.getItem('username');
        check.checked = true;
    }

    check.addEventListener('change', function () {
        if (check.checked) {
            localStorage.setItem('username', ipt.value);
        } else {
            localStorage.removeItem('username');
        }
    })
})