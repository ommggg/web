window.addEventListener('load', function () {
    // touchstart 按下触发
    var touchbox = document.querySelector('.touchbox');
    touchbox.addEventListener('touchstart', function (e) {
        console.log('摸了');
    })
    touchbox.addEventListener('touchmove', function (e) {
        console.log('摩擦摩擦');
    })
    touchbox.addEventListener('touchend', function () {
        console.log('啊');
    })


    // 拖拽事件
    var startX = 0; //一开始手指的 横坐标
    var startY = 0; //一开始手指的 纵坐标
    var boxX = 0; //盒子当前的位置 横坐标
    var boxY = 0; //盒子当前的位置 纵坐标
    var touchbox2 = document.querySelector('.touchbox2');
    touchbox2.addEventListener('touchstart', function (e) {
        startX = e.targetTouches[0].pageX;
        startY = e.targetTouches[0].pageY;
        boxX = touchbox2.offsetLeft;
        boxY = touchbox2.offsetTop;
    });
    touchbox2.addEventListener('touchmove', function (e) {
        var moveX = e.targetTouches[0].pageX - startX;
        var moveY = e.targetTouches[0].pageY - startY;
        touchbox2.style.left = boxX + moveX + 'px';
        touchbox2.style.top = boxY + moveY + 'px';
        e.preventDefault();
    });
    touchbox2.addEventListener('touchend', function (e) {});

});