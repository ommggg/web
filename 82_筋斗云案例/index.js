window.addEventListener('load', function () {
    var nav = document.querySelector('.nav');
    var cloud = document.querySelector('.cloud');
    var lis = nav.querySelectorAll('li');
    var pagenum = 0;
    for (var i = 0; i < lis.length; i++) {
        lis[i].setAttribute('index', i);
        lis[i].addEventListener('mouseenter', function () {
            // cloud.style.left = this.offsetLeft+'px';
            animate(cloud, this.offsetLeft);
        })
        lis[i].addEventListener('mouseleave', function () {
            console.log(lis[pagenum].offsetLeft);
            animate(cloud, lis[pagenum].offsetLeft);
        })
        lis[i].addEventListener('click', function () {
            pagenum = this.getAttribute('index');
        })
    }
})