window.addEventListener('load', function () {
    var focus = document.querySelector('.focus');
    var ul = focus.children[0]; //图片ul
    var ol = focus.children[1]; //小圆点ol
    var w = focus.offsetWidth; //focus的宽度
    var index = 0; //页号
    var statusX = 0; //初始的位置
    var moveX = 0; //需要移动到的位置
    var translatx = -index * w; //需要移动到的位置
    var flag = false;

    var timer = setInterval(function () {
        index++;
        translatx = -index * w;
        ul.style.transition = 'all .3s';
        ul.style.transform = 'translate(' + translatx + 'px)';
    }, 2000);

    ul.addEventListener('transitionend', function () {
        if (index == 3) {
            index = 0;
            translatx = -index * w;
            ul.style.transition = 'none';
            ul.style.transform = 'translate(' + translatx + 'px)';
        }
        if (index < 0) {
            index = 2;
            translatx = -index * w;
            ul.style.transition = 'none';
            ul.style.transform = 'translate(' + translatx + 'px)';

        }
        ol.querySelector('.current').classList.remove('current');
        ol.children[index].classList.add('current');
    })



    ul.addEventListener('touchstart', function (e) {
        statusX = e.targetTouches[0].pageX;
        clearInterval(timer);
    })
    ul.addEventListener('touchmove', function (e) {
        flag = true;
        moveX = e.targetTouches[0].pageX - statusX;
        var translatx = -index * w + moveX;
        ul.style.transition = 'none';
        ul.style.transform = 'translate(' + translatx + 'px)';
        e.preventDefault();
    })
    ul.addEventListener('touchend', function () {
        if (flag) {
            if (Math.abs(moveX) >= 50) {
                // 移动距离大于50为向右滑动，应播放左侧的图
                if (moveX > 0) {
                    index--;
                } else {
                    index++;
                }
                translatx = -index * w;
                ul.style.transition = 'all .3s';
                ul.style.transform = 'translate(' + translatx + 'px)';
            } else {
                translatx = -index * w;
                ul.style.transition = 'all .3s';
                ul.style.transform = 'translate(' + translatx + 'px)';

            }
        }
        flag = false;
        clearInterval(timer);
        timer = setInterval(function () {
            index++;
            translatx = -index * w;
            ul.style.transition = 'all .3s';
            ul.style.transform = 'translate(' + translatx + 'px)';
        }, 2000);
    })

    // 返回顶部模块
    var top = document.querySelector('.top');
    var nav = document.querySelector('nav');
    window.addEventListener('scroll', function () {
        if (window.pageYOffset > nav.offsetTop) {
            top.style.display = 'block';
        } else {
            top.style.display = 'none';
        }
    })
    top.addEventListener('click', function () {
        window.scroll(0, 0);
    })
})